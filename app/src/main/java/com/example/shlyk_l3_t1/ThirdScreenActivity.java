package com.example.shlyk_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ThirdScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_screen);
    }

    public void onStartFourthScreenActivity(View view){
        Intent intent = new Intent(this, FourthScreenActivity.class);
        startActivity(intent);
    }
}
