package com.example.shlyk_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class SecondScreenActivity extends AppCompatActivity {

    SharedPreferences sp;
    EditText editEmailText;
    EditText editPasswordText;
    CheckBox checkBox;
    String editEmail;
    String editPassword;
    SharedPreferences.Editor e;

    public static final String APP_PREFERENCES = "sighininformation";
    public static final String APP_PREFERENCES_EMAIL = "editEmail";
    public static final String APP_PREFERENCES_PASSWORD = "editPassword";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_screen);
        editEmailText = findViewById(R.id.editEmail);
        editPasswordText = findViewById(R.id.editPassword);
        editEmail = editEmailText.getText().toString();
        editPassword = editPasswordText.getText().toString();

        sp = getSharedPreferences(APP_PREFERENCES,
                Context.MODE_PRIVATE);
    }

    public void onStartFourthActivity(View view){
        if(checkRemember()){
            saveInformation();}

            Intent intent = new Intent(this, FourthScreenActivity.class);
            startActivity(intent);
        }


    public boolean checkRemember(){
        checkBox = findViewById(R.id.checkBox);
        if(isEmpty() && checkBox.isChecked()){
            return true;
        } else{
            return false;}
    }

    public boolean isEmpty(){

        if(editEmailText.equals("") || editPasswordText.equals("")){
            Toast.makeText(this, "Fill in the fields", Toast.LENGTH_SHORT).show();
            return false;
        } else{
            return true;
        }
    }

    private void saveInformation(){
        e = sp.edit();
        e.putString(APP_PREFERENCES_EMAIL, editEmail);
        e.putString(APP_PREFERENCES_PASSWORD, editPassword);
        e.apply();
    }
}
