package com.example.shlyk_l3_t1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.textservice.TextInfo;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class FourthScreenActivity extends AppCompatActivity {

    public static final String APP_PREFERENCES = "sighininformation";
    public static final String APP_PREFERENCES_EMAIL = "editEmail";
    public static final String APP_PREFERENCES_PASSWORD = "editPassword";

    SharedPreferences sp;
    SharedPreferences.Editor e;
    Button logoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth_screen);

        RecyclerView rv = findViewById(R.id.recycler);

        DataAdapter dataAdapter = new DataAdapter();
        rv.setAdapter(dataAdapter);

        logoutButton = findViewById(R.id.logoutButton);
        sp = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

    }

    public void Logout(View view){

        e = sp.edit();
        e.remove(APP_PREFERENCES_EMAIL);
        e.remove(APP_PREFERENCES_PASSWORD);
        e.remove(APP_PREFERENCES);
        e.apply();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }


}
