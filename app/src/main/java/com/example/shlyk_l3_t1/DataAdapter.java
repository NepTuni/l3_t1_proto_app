package com.example.shlyk_l3_t1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.textservice.TextInfo;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.HistoryViewHolder> {
    public static class HistoryViewHolder extends RecyclerView.ViewHolder{
        CardView historyCardView;
        TextView titleView;
        TextView textView;

        HistoryViewHolder(final View itemView){
            super(itemView);
            historyCardView = (CardView) itemView.findViewById(R.id.cardView);
            titleView = (TextView)itemView.findViewById(R.id.titleText);
            textView = (TextView) itemView.findViewById(R.id.textView);
            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public  void onClick(View view){
                    itemView.getContext().startActivity(new Intent(itemView.getContext(),FifthScreenActivity.class));
                }
            });
        }
    }

    @Override
    public int getItemCount(){
        return 20;
    }



    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_info, viewGroup,false);
        HistoryViewHolder hVH = new HistoryViewHolder(view);
        return hVH;
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder historyViewHolder, int i) {

        historyViewHolder.titleView.setText(R.string.title_card_view);
        historyViewHolder.textView.setText(R.string.text_card_view);
    }



    public void onAttachedToRecyclerView(RecyclerView recyclerView){
        super.onAttachedToRecyclerView(recyclerView);
    }
}
