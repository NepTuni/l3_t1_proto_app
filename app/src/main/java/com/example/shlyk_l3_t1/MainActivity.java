package com.example.shlyk_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import java.io.File;

public class MainActivity extends AppCompatActivity {


    SharedPreferences sp;
    SharedPreferences.Editor e;

    public static final String APP_PREFERENCES = "sighininformation";
    public static final String APP_PREFERENCES_EMAIL = "editEmail";
    public static final String APP_PREFERENCES_PASSWORD = "editPassword";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sp = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);



    }

    public void onStartSecondScreenActivity(View view){
        if(sp.contains(APP_PREFERENCES_EMAIL) && sp.contains(APP_PREFERENCES_PASSWORD)) {
            Intent intentSkip = new Intent(this, FourthScreenActivity.class);
            startActivity(intentSkip);
        }else{
        Intent intent = new Intent(this, SecondScreenActivity.class);
        startActivity(intent);

    }}

    public void onStartThirdScreenActivity(View view){
        Intent intent = new Intent(this, ThirdScreenActivity.class);
        startActivity(intent);
    }
}
