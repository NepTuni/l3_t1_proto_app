package com.example.shlyk_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

public class FifthScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth_screen);
    }

    public void onButtonClick(View view) {
        Toast toast = Toast.makeText(getApplicationContext(),
                "Was added to favourites",
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM, 0, 170);
        toast.show();
    }
}
